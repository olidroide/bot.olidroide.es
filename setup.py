#!/usr/bin/env python
import os
import re
from setuptools import setup, find_packages

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "configuration.settings")


def parse_requirements(file_name):
    requirements = []
    for line in open(file_name, 'r').read().split('\n'):
        if re.match(r'(\s*#)|(\s*$)', line):
            continue
        if re.match(r'\s*-e\s+', line):
            # TODO support version numbers
            requirements.append(re.sub(r'\s*-e\s+.*#egg=(.*)$', r'\1', line))
        elif re.match(r'\s*-f\s+', line):
            pass
        else:
            requirements.append(line)

    return requirements


setup(
    name='bot.olidroide.es',
    version='0.0.1',
    description="bot olidroide",
    long_description=open(os.path.join(os.path.dirname(__file__), 'README.md')).read(),
    author=['olidroide'],
    author_email='bot@olidroide.es',
    url='https://bitbucket.org/olidroide/bot.olidroide.es',
    keywords="bot olidroide django python",
    license='Olidroide',
    packages=find_packages(exclude=('*.tests', '*.tests.*', 'tests.*', 'tests')),
    classifiers=(
        'Development Status :: 1 - Beta',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU Affero General Public License v3',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Framework :: Django',
    ),
    zip_safe=True,
    install_requires=parse_requirements('requirements.txt'),
)
