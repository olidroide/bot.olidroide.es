#!/bin/bash

virtualenv env
source env/bin/activate
python setup.py install
python manage.py clearsessions
python manage.py makemigrations
python manage.py migrate
python manage.py runserver 0.0.0.0:8000
